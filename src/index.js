import http from 'http'

import example from './example'

import('dotenv/config')

/**
   * This is a function for example template.
   * @param {Object} req - Request
   * @param {Object} res - Response
   * @returns {Object} - Return a object
   * */
const requestListener = function (req, res) {
  res.writeHead(200)
  res.end(example())
}

const server = http.createServer(requestListener)
const port = process.env.PORT || 3000

server.listen(port)
