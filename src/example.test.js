import example from './example'

describe('example', function () {
  test('Should be a function', function () {
    expect(example).toBeInstanceOf(Function)
  })

  test('Should return a text', function () {
    expect(example()).toEqual('It is working!')
  })
})
