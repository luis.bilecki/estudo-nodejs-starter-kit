module.exports = {
  testEnvironment: 'node',
  transform: {
    '.(js|jsx|ts|tsx)': '@sucrase/jest-plugin'
  },
  testPathIgnorePatterns: [
    '/node_modules/',
    'docs'
  ]
}
